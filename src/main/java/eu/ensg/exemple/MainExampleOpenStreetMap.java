package eu.ensg.exemple;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import eu.ensg.osm.Direction;
import eu.ensg.osm.DirectionSimple;
import eu.ensg.osm.Distance;
import eu.ensg.osm.HttpClientOsm;

public class MainExampleOpenStreetMap {
	
	
	private static double lon_point_dec = 2.5852910672180798;
	private static double lat_point_dec = 48.83814042897463;
	private static double lon_prec = 2.586069592597218;
	private static double lat_prec = 48.83800154957097;
	private static double lon_suiv = 2.5849236323428793;
	private static double lat_suiv = 48.83909341863363;
	
	private static double distance_steps;
	private static double duration;
	
	public static void main(String[] args) throws ParserConfigurationException {
		
		double E = lon_point_dec + 0.0002;
		double O = lon_point_dec - 0.0002;
		double S = lat_point_dec - 0.0002;
		double N = lat_point_dec + 0.0002;
			
		
		String dataRequest = "<osm-script>"
				+ "<union>"
				+ "<query type=\"node\">"
				+ "<bbox-query e=\"" + E + "\" n=\"" + N + "\" s=\"" + S + "\" w=\"" + O + "\" />"
				+ "</query>"
				+ "</union>"
				+ "<print mode=\"meta\"/>"
				+ "</osm-script>";
		String xmldata = HttpClientOsm.getOsmXML(dataRequest);
		// System.out.println(xml);
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		try {
			Document doc = builder.parse(new ByteArrayInputStream(xmldata.getBytes()));
			doc.getDocumentElement().normalize();
		    Element root = (Element) doc.getElementsByTagName("osm").item(0);
		
		    int nbNoeuds = root.getElementsByTagName("node").getLength();
		    
		    
		    double lat0 = 0;
		    double lon0 = 0;
	    	
	    	double dist0 = 300000000;
	    	
	    	String valpro = "nol";
	    	  
		    for (int i = 0; i < nbNoeuds; i++) {

		    	Element elem = (Element) root.getElementsByTagName("node").item(i);

		    	// On récupère son ID
		    	long id = Long.valueOf(elem.getAttribute("id"));

		    	// on récupère sa géométrie
		    	double lat = Double.valueOf(elem.getAttribute("lat"));
		    	double lon = Double.valueOf(elem.getAttribute("lon"));
		    	
		    	
		    	for (int j = 0; j < elem.getElementsByTagName("tag").getLength(); j++) {
		    		
					Element tagElem = (Element) elem.getElementsByTagName("tag").item(j);
			
					String cle = tagElem.getAttribute("k");
					String val = tagElem.getAttribute("v");
					
					double distPointj = Distance.dist(lat,lat_point_dec,lon,lon_point_dec);
					
					if (cle.equals("name")) {
						if (distPointj < dist0) {
							dist0 = distPointj;
							valpro = val;
							lon0 = lon;
							lat0 = lat;
							
						}
						
					}
						
		    	}
		    	
		    }
		    
		    
		    Direction direct1 = new Direction(lat_point_dec,lon_point_dec,lat_prec,lon_prec,lat0,lon0);
		    DirectionSimple d1 = new DirectionSimple(lat_point_dec,lon_point_dec,lat_prec,lon_prec,lat_suiv,lon_suiv);
		    

		    System.out.println(lat0 + "," + lon0);
		    System.out.println(d1.toString() + direct1.toString() + valpro + " puis continuer sur " + distance_steps + " m");
		    System.out.println("Le temps total de trajet est : " + duration + " minute(s)");
		    System.out.println(direct1.toString2());
	    	System.out.println("Les coordonnées du point de référence sont : [" + lat0 + "," + lon0 + "]");
	    	
	    	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	
		
	}
	
	
	
}

