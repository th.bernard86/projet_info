package eu.ensg.exemple;

import java.io.ByteArrayInputStream;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.ensg.ign.*;
import eu.ensg.osm.*;

public class MainFinal {
	
	public static void main(String[] args) throws ParserConfigurationException, InterruptedException {
		
		String start = "2.3547965564258226,48.84615526548436";
		String end = "2.3698762993655165,48.84911418041841";
		
		String url = "https://wxs.ign.fr/calcul/geoportail/itineraire/rest/1.0.0/route?resource=bdtopo-pgr"
				+ "&profile=pedestrian&optimization=fastest"
				+ "&start="+start
				+ "&end="+end
				+ "&intermediates=&constraints={\"constraintType\":\"banned\",\"key\":\"wayType\",\"operator\":\"=\",\"value\":\"tunnel\"}"
				+ "&geometryFormat=geojson&crs=EPSG:4326&getSteps=true&getBbox=true&waysAttributes=nature";
	  
		String txtJson = HttpClientIgn.request(url);
		
		Gson gson = new GsonBuilder().create();
		Resultat itineraire = gson.fromJson(txtJson, Resultat.class);
		
        for (Portion portion : itineraire.getPortions()) {
			System.out.println(portion.getDuration());	
		
			int p=0;
			for (Step steps : portion.getSteps()) {
				
				List<Double[]> coordinates = steps.getGeometry().getCoordinates();
				
				double lonDecision;
				double latDecision;
				double lonApres;
				double latApres;
				double lonAvant;
				double latAvant;
				
				if (coordinates.size() == 1) { //si le step n'est composé que d'un seul coord
					
					lonDecision = coordinates.get(0)[0];  //Point de decision
			     	latDecision = coordinates.get(0)[1];
			     
			     	List<Double[]> coord2 = portion.getSteps().get(p+1).getGeometry().getCoordinates();
			     	lonApres = coord2.get(0)[0];            //Point apres le point de decision
			     	latApres = coord2.get(0)[1];
			     	
			     	List<Double[]> coord1 = portion.getSteps().get(p-1).getGeometry().getCoordinates();
			     	lonAvant = coord1.get(coord1.size()-1)[0];       //Point avant le point de decision
			     	latAvant = coord1.get(coord1.size()-1)[1];
						
				}
				
				else {
			     	List<Double[]> coord1 = steps.getGeometry().getCoordinates();
			    	lonAvant = coord1.get(coord1.size()-2)[0];       //Point avant le point de decision
					latAvant = coord1.get(coord1.size()-2)[1];
				 
					lonDecision = coord1.get(coord1.size()-1)[0];    //Point de decision
			        latDecision = coord1.get(coord1.size()-1)[1];
				   
		     	    List<Double[]> coord = portion.getSteps().get(p+1).getGeometry().getCoordinates();
		    	    lonApres = coord.get(0)[0];                      //Point le point de decision
		    	    latApres = coord.get(0)[1];	
		    		    
			   	}
				
				double E = lonDecision + 0.0002;
    			double O = lonDecision - 0.0002;
    			double S = latDecision - 0.0002;
    			double N = latDecision + 0.0002;
				
				String dataRequest = "<osm-script>"
					+ "<union>"
					+ "<query type=\"node\">"
					+ "<bbox-query e=\"" + E + "\" n=\"" + N + "\" s=\"" + S + "\" w=\"" + O + "\" />"
					+ "</query>"
					+ "</union>"
					+ "<print mode=\"meta\"/>"
					+ "</osm-script>";
		    	String xmldata = HttpClientOsm.getOsmXML(dataRequest);
				
		    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    		factory.setNamespaceAware(true);
		    	DocumentBuilder builder = factory.newDocumentBuilder();
	     		try {
				Document doc = builder.parse(new ByteArrayInputStream(xmldata.getBytes()));
				doc.getDocumentElement().normalize();
			    Element root = (Element) doc.getElementsByTagName("osm").item(0);
				
			    int nbNoeuds = root.getElementsByTagName("node").getLength();
				    
				    
			    double lat0 = 0;
			    double lon0 = 0;
			    	
		    	double dist0 = 300000000;
			    	
		    	String valpro = "nol";
			    	  
			    for (int i = 0; i < nbNoeuds; i++) {

			    	Element elem = (Element) root.getElementsByTagName("node").item(i);

			    	// On récupère son ID
			    	long id = Long.valueOf(elem.getAttribute("id"));

			    	// on récupère sa géométrie
			    	double lat = Double.valueOf(elem.getAttribute("lat"));
			    	double lon = Double.valueOf(elem.getAttribute("lon"));
				    	
				    	
				   	for (int j = 0; j < elem.getElementsByTagName("tag").getLength(); j++) {
				    		
						Element tagElem = (Element) elem.getElementsByTagName("tag").item(j);
					
						String cle = tagElem.getAttribute("k");
						String val = tagElem.getAttribute("v");
							
						double distPointj = Distance.dist(lat,latDecision,lon,lonDecision);
							
						if (cle.equals("name")) {
							if (distPointj < dist0) {
								dist0 = distPointj;				
								valpro = val;
								lon0 = lon;
								lat0 = lat;
									
								}
								
							}
								
				    	}
				    	
				    }
				    
				    
				    Direction direct1 = new Direction(latDecision,lonDecision,latAvant,lonAvant,lat0,lon0);
				    DirectionSimple d1 = new DirectionSimple(latDecision,lonDecision,latAvant,lonAvant,latApres,lonApres);
				    

		  	   	//System.out.println(lat0 + "," + lon0);
				    System.out.println(d1.toString() + direct1.toString() + valpro + " puis continuer sur " + "distance_steps" + " m");
		         //System.out.println("Le temps total de trajet est : " + "duration" + " minute(s)");
			   	//System.out.println(direct1.toString2());
			     //System.out.println("Les coordonnées du point de référence sont : [" + lat0 + "," + lon0 + "]");
			    	
			    	
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			
			    Thread.sleep(25000);
				p+=1;          
		    	}
				
				
			    
				
	}
	}
	}	 
			
		
	

