package eu.ensg.osm;

public class TestMain {
	
	public static void main(final String[] args) {
		
		double lat1 = 51.5007; 
		double lon1 = 0.1246; 
		
		double lat2 = 40.6892; 
		double lon2 = 74.0445;
		
		Distance dist1 = new Distance(lat1,lon1,lat2,lon2);
		
		dist1.dist(lat1, lon1, lat2, lon2);
		System.out.print(dist1.toString() + "\n");
		
		Direction direct1 = new Direction(lat1,lon1,lat2,lon2, lon2, lon2);
		
		System.out.println(direct1.toString());
		
	}

	

}
