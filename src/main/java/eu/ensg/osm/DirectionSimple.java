package eu.ensg.osm;

public class DirectionSimple {
	
	private double lat_point_dec;
	private double lon_point_dec;
	private double lat_prec;
	private double lon_prec;
	private double lat_suiv;
	private double lon_suiv;
	private double angle;
	private String text;
	
	public DirectionSimple(double lat_point_dec,double lon_point_dec,double lat_prec,double lon_prec,double lat_suiv,double lon_suiv) {
		this.lat_point_dec = lat_point_dec;
		this.lon_point_dec = lon_point_dec;
		this.lat_prec = lat_prec;
		this.lon_prec = lon_prec;
		this.lat_suiv = lat_suiv;
		this.lon_suiv = lon_suiv;
		
		this.angle = getAngleFromThreePoints(lat_point_dec,lon_point_dec,lat_prec,lon_prec,lat_suiv,lon_suiv);
		
		this.text = choixDirection(this.angle);
		
	}
	
	public static double getAngleFromThreePoints(double lat_point_dec,double lon_point_dec,double lat_prec,double lon_prec,double lat_suiv,double lon_suiv) {
		
		
		//double dist1 = Distance.dist(lat_prec,lon_prec,lat_point_dec,lon_point_dec);
		//double dist2 = Distance.dist(lat_point_dec,lon_point_dec,lat_suiv,lon_suiv);
		//double dist3 = Distance.dist(lat_prec,lon_prec,lat_suiv,lon_suiv);

		double angle1 = Math.atan2(lat_prec - lat_point_dec, lon_prec - lon_point_dec);
		double angle2 = Math.atan2(lat_suiv - lat_point_dec, lon_suiv - lon_point_dec);
		
		double angle3 = (angle2 - angle1);
		
		if(angle3<0) {
			return angle3*180/Math.PI + 360;
		}
		else{
			return angle3*180/Math.PI;
		}		
	}
	
	public String choixDirection(double angle2) {
		
		System.out.println("Angle Direction simple : " + angle2);
		
		if(angle2 > 22.5 && angle2 < 67.5) {
			return "Tourner directement à droite ";
		}
		else if(angle2 > 67.5 && angle2 < 112.5) {
			return "Tourner à droite ";
		}
		else if(angle2 > 112.5 && angle2 < 157.5) {
			return "Diriger vous vers la droite ";
		}
		else if(angle2 > 157.5 && angle2 < 202.5) {
			return "Continuer tout droit ";
		}
		else if(angle2 > 202.5 && angle2 < 247.5) {
			return "Diriger vous vers la gauche ";
		}
		else if(angle2 > 247.5 && angle2 < 292.5) {
			return "Tourner à gauche ";
		}
		else if(angle2 > 292.5 && angle2 < 337.5) {
			return "Tourner directement à gauche ";
		}
		else if((angle2 > 337.5 && angle2 < 360) || (angle2 > 0 && angle2 <22.5)) {
			return "Faites demi-tour ";
		}
	return "ah ";
	}
	
	@Override
	public String toString() {
		return this.text;
	}
	
}
