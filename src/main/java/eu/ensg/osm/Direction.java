package eu.ensg.osm;

import org.locationtech.jts.geom.Point;

public class Direction {
	
	private double lat_point_dec;
	private double lon_point_dec;
	private double lat_prec;
	private double lon_prec;
	private double lat_suiv;
	private double lon_suiv;
	
	private double angle;
	private String text;
	
	public Direction(double lat_point_dec,double lon_point_dec,double lat_prec,double lon_prec,double lat_suiv,double lon_suiv) {
		
		this.lat_point_dec = lat_point_dec;
		this.lon_point_dec = lon_point_dec;
		this.lat_prec = lat_prec;
		this.lon_prec = lon_prec;
		this.lat_suiv = lat_suiv;
		this.lon_suiv = lon_suiv;
		
		this.angle = DirectionSimple.getAngleFromThreePoints(lat_point_dec,lon_point_dec,lat_prec,lon_prec,lat_suiv,lon_suiv);
		this.text = choixDirection(this.angle);
	}
		
	public Direction(double lat_point_dec,double lon_point_dec,double lat_prec,double lon_prec,double lat_suiv,double lon_suiv, double angle) {
		
		this.angle = angle;
		
		this.lat_point_dec = lat_point_dec;
		this.lon_point_dec = lon_point_dec;
		this.lat_prec = lat_prec;
		this.lon_prec = lon_prec;
		this.lat_suiv = lat_suiv;
		this.lon_suiv = lon_suiv;
	}
		
	
	
	
	//public float getAngleOfLineBetweenTwoPoints(double x1, double y1, double x2, double y2) {
	//	double xDiff = x2 - x1;
	//	double yDiff = y2 - y1;
    //    return (float) Math.abs(Math.atan2(yDiff, xDiff) * (180 / Math.PI));
	//}
	
	
	double toRad(double angdeg) {
		return Math.toRadians(angdeg);
	}
	
	public String choixDirection(double angle2) {
		
		
		System.out.println("Angle Direction : " + angle2);
		
		if(angle2 > 22.5 && angle2 < 67.5) {
			return "avant ";
		}
		else if(angle2 > 67.5 && angle2 < 112.5) {
			return "à ";
		}
		else if(angle2 > 112.5 && angle2 < 157.5) {
			return "après ";
		}
		else if(angle2 > 157.5 && angle2 < 202.5) {
			return "vers ";
		}
		else if(angle2 > 202.5 && angle2 < 247.5) {
			return "après ";
		}
		else if(angle2 > 247.5 && angle2 < 292.5) {
			return "à ";
		}
		else if(angle2 > 292.5 && angle2 < 337.5) {
			return "avant ";
		}
		else if((angle2 > 337.5 && angle2 < 360) || (angle2 > 0 && angle2 <22.5)) {
			return "vers ";
		}
		return "il y a une erreur ";
	}
	
	@Override
	public String toString() {
		return this.text; 	
	}
	public String toString2() {
		return  "L'angle entre l'endroit d'où l'on vient et la référence vaut " + this.angle + " degrés.";
	}
}
