package eu.ensg.osm;

public class Distance {
	
	private double lat_x;
	private double lat_y;
	private double long_x;
	private double long_y;
	
	public Distance(double lat_x, double long_x, double lat_y, double long_y) {
		this.lat_x = lat_x;
		this.long_x = long_x;
		this.lat_y = lat_y;
		this.long_y = long_y;
	}
	
	static double toRad(double angdeg) {
		return Math.toRadians(angdeg);
	}
	
	public static double dist(double lat1, double lon1, double lat2, double lon2) { 
		
		//  
		double dLat = toRad(lat2 - lat1); 
		double dLon = toRad(lon2 - lon1); 

		// Convertit en radians les latitudes 
		lat1 = toRad(lat1); 
		lat2 = toRad(lat2); 

		// On applique la formule
		double a = Math.pow(Math.sin(dLat / 2), 2) +  Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1) * Math.cos(lat2); 
		double c = 2 * Math.asin(Math.sqrt(a));
		double rad = 6371; 
		
		return rad * c;
	}
	
	@Override
	public String toString() {
		return "les points [" + lat_x +";"+long_x +"] et ["+lat_y + ";" + long_y + "] sont éloignés de "+ dist(lat_x, long_x, lat_y, long_y) + " m";
	}
	
}
