package eu.ensg.ign;

public class Step {
	
	    private double distance;
		public double getDistance() { return this.distance; }
		public void setDistance(double distance) { this.distance = distance; }
		
		private double duration;
		public double getDuration() { return this.duration; }
		public void setDuration(double duration) { this.duration = duration; }
		
		private Geometrie geometry;
		public Geometrie getGeometry() {return geometry;}
		public void setGeometry(Geometrie geometry) {this.geometry = geometry;}

		

}
