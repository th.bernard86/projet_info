package eu.ensg.ign;

import java.util.List;

public class Resultat {
	
	private String profile;
	public void setProfile(String profile) { this.profile = profile; }
	public String getProfile() { return this.profile; }
	
	private String start;
	public void setStart(String start) { this.start = start; }
	public String getStart() { return start; }
	
	private String end;
	public void setEnd(String end) { this.end = end; }
	public String getEnd() { return end; }
	
	private List<Portion> portions;
	public List<Portion> getPortions() { return this.portions; }
	public void setPortions(List<Portion> portions) { this.portions = portions; }
	
	
}
